<?php
require './connect.php';

$method = $_GET['method'];
$role = $_GET['role'];
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: *');
header("Content-type: application/json; charset=UTF-8");

if ($role === 'แอดมิน' && $method === 'insert_hospital') {
    $postdata = file_get_contents("php://input");
    $data = json_decode($postdata);

    $h_id = $data->h_id;
    $hos_name = $data->hos_name;
    $address = $data->address;
    $phone = $data->phone;
    $freeT3_standard = $data->freeT3_standard;
    $freeT4_standard = $data->freeT4_standard;
    $TSH_standard = $data->TSH_standard;
    $TRAb_standard = $data->TRAb_standard;

    $sql = "SELECT * FROM Hospital WHERE hos_name = ? ";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param('s', $hos_name);
    $stmt->execute();
    $result = $stmt->get_result();
    if ($result->num_rows > 0) {
        echo json_encode(array("result" => "hospital name already exist."));
    } else {
        if ($hos_name) {
            $sql = "INSERT INTO Hospital (id, h_id, hos_name, address, phone, freeT3_standard,
                freeT4_standard, TSH_standard, TRAb_standard) VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?)";
            $stmt = $conn->prepare($sql);
            $stmt->bind_param('ssssdddd', $h_id, $hos_name, $address, $phone, $freeT3_standard, $freeT4_standard, $TSH_standard, $TRAb_standard);
            $error = false;
            $error = $stmt->execute();
            if ($error) {
                echo json_encode(array("result" => "ดำเนินการบันทึกข้อมูลเสร็จสิ้น"));
            } else {
                echo json_encode(array("result" => "Fail"));
            }
        }
    }
}

if ($role === 'แอดมิน' && $method === 'update_hospital') {
    $postdata = file_get_contents("php://input");
    $data = json_decode($postdata);
    $id = $data->id;

    $h_id = $data->h_id;
    $hos_name = $data->hos_name;
    $address = $data->address;
    $phone = $data->phone;
    $freeT3_standard = $data->freeT3_standard;
    $freeT4_standard = $data->freeT4_standard;
    $TSH_standard = $data->TSH_standard;
    $TRAb_standard = $data->TRAb_standard;

    $error = false;
    $sql = "UPDATE Hospital SET h_id=? ,hos_name=?,address=?,phone=?,freeT3_standard=?,freeT4_standard=?,TSH_standard=?,TRAb_standard=? WHERE id=?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param('ssssddddi', $h_id, $hos_name, $address, $phone, $freeT3_standard, $freeT4_standard, $TSH_standard, $TRAb_standard, $id);
    $error = $stmt->execute();
    if ($error) {
        echo json_encode(array("result" => "ดำเนินการบันทึกข้อมูลเสร็จสิ้น"));
    } else {
        echo json_encode(array("result" => "Fail"));
    }
}

if ($role === 'แอดมิน' && $method === 'delete_hospital') {
    $postdata = file_get_contents("php://input");
    $data = json_decode($postdata);
    $id = $data->id;
    $sql = "DELETE FROM Hospital WHERE id = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param('i', $id);
    $error = $stmt->execute();
    if ($error) {
        echo json_encode(array("result" => "ดำเนินการลบข้อมูลโรงพยาบาลเสร็จสิ้น"));
    } else {
        echo json_encode(array("result" => "Fail"));
    }
}

if (($role === 'พยาบาล' || $role === 'แอดมิน' || $role === 'หมอ') && $method === 'get_hospital') {
    $postdata = file_get_contents("php://input");
    $data = json_decode($postdata);
    $sql = "SELECT * FROM Hospital";
    $result = $conn->query($sql);

    $resultArray = array();
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            array_push($resultArray, $row);
        }
        echo json_encode($resultArray);
    } else {
        echo json_encode(array("result" => "Fail"));
    }
}

if ($role === 'แอดมิน' && $method === 'insert_user') {
    $postdata = file_get_contents("php://input");
    $data = json_decode($postdata);
    $idCard = $data->idcard;
    $title = $data->title;
    $firstname = $data->firstname;
    $lastname = $data->lastname;
    $dof = $data->dof;
    $gender = $data->gender;
    $national = $data->national;
    $status = $data->status;
    $phone = $data->phone;

    $patient = $data->patient;
    $doctor = $data->doctor;
    $nurse = $data->nurse;

    $sql = "SELECT * FROM User_profile WHERE person_id = ? ";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param('s', $idCard);
    $stmt->execute();
    $result = $stmt->get_result();
    if ($result->num_rows > 0) {
        echo json_encode(array("result" => "$idCard is registered."));
    } else {
        $sql = "INSERT INTO User_profile (person_id,title,firstname,lastname,birthdate,gender,nationality,status,phone) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param('sssssssss', $idCard, $title, $firstname, $lastname, $dof, $gender, $national, $status, $phone);
        $error = false;
        $error = $stmt->execute();
        if ($error) {
            if ($patient == 'add') {
                $sql = "INSERT INTO Role (id,role_name,person_id) VALUES (NULL,'ผู้ป่วย', ?)";
                $stmt = $conn->prepare($sql);
                $stmt->bind_param('s', $idCard);
                $error = $stmt->execute();

                if ($error) {
                    $sql = "INSERT INTO Patient_Summary (person_id,rou_id,volume_result,iodine_result) VALUES (?,'1',NULL,NULL)";
                    $stmt = $conn->prepare($sql);
                    $stmt->bind_param('s', $idCard);
                    $error = $stmt->execute();
                } else {
                    echo json_encode(array("result" => "Fail"));
                }

            }
            if ($doctor == 'add') {
                $sql = "INSERT INTO Role (id,role_name,person_id) VALUES (NULL,'หมอ', ?)";
                $stmt = $conn->prepare($sql);
                $stmt->bind_param('s', $idCard);
                $error = $stmt->execute();
                if ($error) {

                } else {
                    echo json_encode(array("result" => "Fail"));
                }
            }
            if ($nurse == 'add') {
                $sql = "INSERT INTO Role (id,role_name,person_id) VALUES (NULL,'พยาบาล', ?)";
                $stmt = $conn->prepare($sql);
                $stmt->bind_param('s', $idCard);
                $error = $stmt->execute();
                if ($error) {

                } else {
                    echo json_encode(array("result" => "Fail"));
                }
            }
            $sql = "INSERT INTO Authen (username,password) VALUES (?,?)";
            $stmt = $conn->prepare($sql);
            $stmt->bind_param('ss', $idCard, $idCard);
            $error = $stmt->execute();
            if ($error) {
                echo json_encode(array("username" => $idCard, "password" => $idCard));
            } else {
                echo json_encode(array("result" => "Fail"));
            }

        } else {
            echo json_encode(array("result" => "Fail"));
        }
    }
}

if ($role === 'แอดมิน' && $method === 'patient_rpt') {
    $postdata = file_get_contents("php://input");
    $data = json_decode($postdata);
    $sql = "SELECT br1.person_id as CID, br1.rou_id as round, 
    CONCAT(up.title, up.firstname,' ', up.lastname) AS fullname, up.birthdate, up.gender as sex, up.nationality as race, up.status, up.phone,
    hs.from_hn as from_hos, hs.to_hn as to_hos, hs.Hos_base_h_id as HN_BUU,
    cp.month, cp.year, cp.comp_status as complication, cp.comp_name as desc_complication, cp.comp_indication as indication , cp.comp_majoradverse as PTUorMMI, cp.comp_description as desc_PTUorMMI,
    br1.check_date as body_date, br1.sweat, br1.hair_loss as hairloss, br1.body_weight as bodyweight, br1.heart_rate as heartrate, CONCAT(br1.blood_pressure_upper, '/',br1.blood_pressure_lower) AS bloodpressure, br1.eye_detect , br1.eye_result, br1.doctor_name, br1.doctor_date as eye_date, br1.doctor_result as eye_result, br1.treatment,
    br2.thyroid_size, br2.thyroid_tumor_detect as tumor_detect, br2.thyroid_tumor_size as tumor_size, br2.heart_lung_unusual, br2.heart_lung_detail, br2.trembling_hand, br2.power_left_hand, br2.power_right_hand, br2.power_left_leg, br2.power_right_leg, br2.swell_shin,
    br2.brittle_nail, br2.detail,
    isp.frustration, isp.hard_sleep, isp.eat_a_lot, isp.feel_hot, isp.fast_heartbeat, isp.shaking_hand, isp.goiter, isp.thyroid_lump, isp.bulging_eye, isp.digest_3, isp.lose_weight, isp.weak_arm, isp.few_period, isp.disease_name as disease,
    sp.stress, sp.hard_work, sp.night_work_hour, sp.overtime_hour, sp.sleep_less_than_4, sp.pregnant, CONCAT(sp.smoking_amount, ' ', sp.select_amount) AS smoking_amount, CONCAT(sp.smoking_time, ' ', sp.select_time) AS smoking_time, CONCAT(sp.smoking_stop, ' ', sp.select_stop) AS smoking_stop, sp.no_risk_factor,
    sp.relative_toxic_thyroid as relative_thyroid, sp.relative_name as desc_relative,
    pf.pa_fol_date, pf.pa_fol_result, pf.pa_fol as IodineOrFollow , pf.ttf3_tt3 as fT3, pf.nor_fT3, pf.fT4_result as fT4, pf.nor_fT4, pf.TSH_result as TSH, pf.nor_TSH, pf.TRAb_result as TRAb, pf.nor_TRAb, pf.pa_fol_anti as med_anti, pf.pa_fol_anti_amount as med_anti_daily, pf.pa_fol_beta as med_beta, pf.pa_fol_beta_amount as med_beta_daily,
    ps.volume_result, ps.iodine_result, ps.iodine_result_other as desc_iodine_result,
    pph.method, pph.prep_date, pph.end_date, pph.period_control, pph.birth_control_name as birth_control, CONCAT(pph.birth_control_state,' ', pph.birth_control_time) AS birthcontrol_time, pph.last_period, pph.last_period_amount,
    ud.UPT_date, ud.UPT_result,
    tu.thy_che_date as Ult_date, tu.thy_che_size as Ult_size, tu.thy_che_found as Ult_found, tu.thy_ult_result, 
    tud.thy_num, tud.thy_ult_date, tud.thy_ult_category, CONCAT(tud.thy_ult_advice, ' ', tud.thy_ult_follow_num, ' ', tud.thy_ult_follow_unit, ' ', tud.thy_ult_fine_result) AS thy_ult_advice, tud.thy_ult_surgury_desc as Surgury_desc, 
    CONCAT(tud.thy_ult_ex_advice, ' ', tud.thy_ult_ex_follow_num, ' ', tud.thy_ult_ex_follow_unit, ' ', tud.thy_ult_ex_fine_result) AS thy_ult_ex_advice, tud.thy_ult_ex_surgury_desc as Ex_surgury_desc
    FROM  `Body_result1` br1
    LEFT JOIN  `Body_result2` br2 ON  br1.person_id = br2.person_id AND br1.rou_id = br2.rou_id
    LEFT JOIN `Complication_phase` cp ON br1.person_id = cp.person_id AND br1.rou_id = cp.rou_id
    LEFT JOIN `Hospital_send` hs ON br1.person_id = hs.person_id AND br1.rou_id = hs.rou_id
    LEFT JOIN `Init_symp` isp ON  br1.person_id = isp.person_id AND br1.rou_id = isp.rou_id
    LEFT JOIN `Patient_follow` pf ON br1.person_id = pf.person_id AND br1.rou_id = pf.rou_id 
    LEFT JOIN `Patient_Summary` ps ON br1.person_id = ps.person_id AND br1.rou_id = ps.rou_id
    LEFT JOIN `Prepare_phase` pph ON br1.person_id = pph.person_id AND br1.rou_id = pph.rou_id
    LEFT JOIN `Role` role ON br1.person_id = role.person_id
    LEFT JOIN `Symp_phase` sp ON br1.person_id = sp.person_id AND br1.rou_id = sp.rou_id
    LEFT JOIN `Thyroid_ultrasound` tu ON br1.person_id = tu.person_id AND br1.rou_id = tu.rou_id
    LEFT JOIN `Thyroid_ult_detail` tud ON br1.person_id = tud.person_id AND br1.rou_id = tud.rou_id
    LEFT JOIN `UPT_detail` ud ON br1.person_id = ud.person_id AND br1.rou_id = ud.rou_id
    LEFT JOIN `User_profile` up ON br1.person_id = up.person_id
    WHERE role.role_name = 'ผู้ป่วย'
    ORDER BY  br1.person_id";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    $result = $stmt->get_result();

    $resultArray = array();
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            array_push($resultArray, $row);
        }
        echo json_encode($resultArray);
    } else {
        echo json_encode(array("result" => "Fail"));
    }
}

if ($role === 'แอดมิน' && $method === 'get_user') {
    $postdata = file_get_contents("php://input");
    $data = json_decode($postdata);
    $search = $data->search;
    $sql = "SELECT CONCAT(b.title,b.firstname,' ',b.lastname) as name,
            b.person_id,
            b.title,
            b.firstname,
            b.lastname,
            b.birthdate,
            b.gender,
            b.nationality,
            b.status,
            b.phone
            FROM Role as a
            INNER JOIN User_profile as b
            ON a.person_id = b.person_id
            WHERE a.role_name = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param('s', $search);
    $stmt->execute();
    $result = $stmt->get_result();

    $resultArray = array();
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            array_push($resultArray, $row);
        }
        echo json_encode($resultArray);
    } else {
        echo json_encode(array("result" => "Fail"));
    }
}

if ($role === 'แอดมิน' && $method === 'update_user') {
    $postdata = file_get_contents("php://input");
    $data = json_decode($postdata);
    $idCard = $data->idcard;
    $title = $data->title;
    $firstname = $data->firstname;
    $lastname = $data->lastname;
    $dof = $data->dof;
    $gender = $data->gender;
    $national = $data->national;
    $status = $data->status;
    $phone = $data->phone;

    $patient = $data->patient;
    $doctor = $data->doctor;
    $nurse = $data->nurse;

    $error = false;
    $sql = "UPDATE User_profile SET title=?,firstname=?,lastname=?,birthdate=?,gender=?,nationality=?,status=?,phone=? WHERE person_id=?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param('sssssssss', $title, $firstname, $lastname, $dof, $gender, $national, $status, $phone, $idCard);
    $error = $stmt->execute();
    if ($error) {

        if ($patient == null) {
            $sql = "DELETE FROM Role WHERE person_id = ? AND role_name = 'ผู้ป่วย'";
            $stmt = $conn->prepare($sql);
            $stmt->bind_param('s', $idCard);
            $error = $stmt->execute();
            if ($error) {
                $sql = "DELETE FROM Patient_Summary WHERE person_id = ?";
                $stmt = $conn->prepare($sql);
                $stmt->bind_param('s', $idCard);
                $error = $stmt->execute();
            } else {
                echo json_encode(array("result" => "Fail"));
            }
        }
        if ($patient == "add") {
            $sql = "SELECT * FROM Role WHERE person_id = ? AND role_name = 'ผู้ป่วย'";
            $stmt = $conn->prepare($sql);
            $stmt->bind_param('s', $idCard);
            $stmt->execute();
            $result = $stmt->get_result();
            if ($result->num_rows > 0) {

            } else {
                $sql = "INSERT INTO Role (id,role_name,person_id) VALUES (NULL,'ผู้ป่วย', ?)";
                $stmt = $conn->prepare($sql);
                $stmt->bind_param('s', $idCard);
                $error = $stmt->execute();
                if ($error) {
                    $sql = "INSERT INTO Patient_Summary (person_id,rou_id) VALUES (?,'1')";
                    $stmt = $conn->prepare($sql);
                    $stmt->bind_param('s', $idCard);
                    $error = $stmt->execute();
                } else {
                    echo json_encode(array("result" => "Fail"));
                }
            }

        }

        if ($doctor == null) {
            $sql = "DELETE FROM Role WHERE person_id = ? AND role_name = 'หมอ'";
            $stmt = $conn->prepare($sql);
            $stmt->bind_param('s', $idCard);
            $error = $stmt->execute();
            if ($error) {

            } else {
                echo json_encode(array("result" => "Fail"));
            }
        }
        if ($doctor == "add") {
            $sql = "SELECT * FROM Role WHERE person_id = ? AND role_name = 'หมอ'";
            $stmt = $conn->prepare($sql);
            $stmt->bind_param('s', $idCard);
            $stmt->execute();
            $result = $stmt->get_result();
            if ($result->num_rows > 0) {

            } else {
                $sql = "INSERT INTO Role (id,role_name,person_id) VALUES (NULL,'หมอ', ?)";
                $stmt = $conn->prepare($sql);
                $stmt->bind_param('s', $idCard);
                $error = $stmt->execute();
                if ($error) {

                } else {
                    echo json_encode(array("result" => "Fail"));
                }
            }

        }
        if ($nurse == null) {
            $sql = "DELETE FROM Role WHERE person_id = ? AND role_name = 'พยาบาล'";
            $stmt = $conn->prepare($sql);
            $stmt->bind_param('s', $idCard);
            $error = $stmt->execute();
            if ($error) {

            } else {
                echo json_encode(array("result" => "Fail"));
            }
        }
        if ($nurse == "add") {
            $sql = "SELECT * FROM Role WHERE person_id = ? AND role_name = 'พยาบาล'";
            $stmt = $conn->prepare($sql);
            $stmt->bind_param('s', $idCard);
            $stmt->execute();
            $result = $stmt->get_result();
            if ($result->num_rows > 0) {

            } else {
                $sql = "INSERT INTO Role (id,role_name,person_id) VALUES (NULL,'พยาบาล', ?)";
                $stmt = $conn->prepare($sql);
                $stmt->bind_param('s', $idCard);
                $error = $stmt->execute();
                if ($error) {

                } else {
                    echo json_encode(array("result" => "Fail"));
                }
            }

        }

        echo json_encode(array("result" => "ดำเนินการบันทึกข้อมูลเสร็จสิ้น"));
    } else {
        echo json_encode(array("result" => "Fail"));
    }
}

if ($role === 'แอดมิน' && $method === 'delete_user') {
    $postdata = file_get_contents("php://input");
    $data = json_decode($postdata);
    $idCard = $data->idcard;
    $sql = "DELETE FROM User_profile WHERE person_id = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param('s', $idCard);
    $error = $stmt->execute();
    if ($error) {
        echo json_encode(array("result" => "ดำเนินการลบข้อมูลผู้ใช้เสร็จสิ้น"));
    } else {
        echo json_encode(array("result" => "Fail"));
    }
}

if ($role === 'แอดมิน' && $method === 'getrole_user') {
    $postdata = file_get_contents("php://input");
    $data = json_decode($postdata);
    $idCard = $data->idcard;
    $sql = "SELECT * FROM Role WHERE person_id = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param('s', $idCard);
    $error = $stmt->execute();
    $result = $stmt->get_result();
    $resultArray = array();
    if ($result->num_rows > 0 ) {
         while ($row = $result->fetch_assoc()) {
                array_push($resultArray, $row);
            }
         echo json_encode($resultArray);
    } else {
        echo json_encode(array("result" => "Fail"));
    }
}
